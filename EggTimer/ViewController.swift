//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    //Constants
//    let softTime = 5
//    let mediumTime = 7
//    let hardTime = 12
    
    //time in seconds
    let eggTimes = ["Soft": 3, "Medium": 4, "Hard": 7]
    //var secondsRemaining = 60
    var timer = Timer()
    var totalTime = 0
    var secondsPassed = 0
    
    var player: AVAudioPlayer!
    
    //MARK:- IBAction
    @IBAction func hardnessSelected(_ sender: UIButton) {
        
        //to invalidate/to cancel previous timer and start a new acc to new btn pressed
        timer.invalidate()
        
        guard let hardness = sender.currentTitle else {return} //Soft, Medium, Hard
        
        totalTime = eggTimes[hardness]!
        
        //To reset progress when user press another
        progressBar.progress = 0.0
        secondsPassed = 0
        titleLabel.text = hardness
        
        //Timer code
        timer = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(updateTimer),
            userInfo: nil,
            repeats: true)
        
    }
    
    //MARK:- Helper Methods
    @objc func updateTimer(){
        
        if secondsPassed < totalTime {
            secondsPassed += 1
            progressBar.progress = Float(secondsPassed) / Float(totalTime)
        } else {
            //when egg is done so change label to done
            timer.invalidate()
            titleLabel.text = "Done!"
            playSound()
        }
        
    }
    
    func playSound() {
        let url = Bundle.main.url(forResource: "alarm_sound", withExtension: "mp3")
        player = try! AVAudioPlayer(contentsOf: url!)
        player.play()
    }
    
}
